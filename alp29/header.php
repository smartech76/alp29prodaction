<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package alp29
 */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/ico" href="../favicon.ico"/>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- drawer.css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/drawer/3.2.2/css/drawer.min.css">
    
    <?php wp_head(); ?>
    <!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49010912 = new Ya.Metrika({
                    id:49010912,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/49010912" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</head>
<body class="drawer drawer--left body-main">
<div role="banner">
    <button type="button" class="drawer-toggle drawer-hamburger drawer-container">
        <span class="sr-only">toggle navigation</span>
        <span class="drawer-hamburger-icon"></span>
    </button>
    <div class="drawer-nav" role="navigation">
        <ul class="drawer-menu">
            <li><a target-id="main" class="nav_page drawer-brand" href="<?php echo get_home_url(); ?>#main">Альпинстрой</a></li>
            <li><a target-id="main" class="nav_page drawer-menu-item" id="main-close" href="<?php echo get_home_url(); ?>#main">ГЛАВНАЯ</a></li>
            <li><a target-id="spheres" class="nav_page drawer-menu-item" id="spheres-close" href="<?php echo get_home_url(); ?>#spheres">УСЛУГИ</a></li>
            <li><a target-id="feedbacks" class="nav_page drawer-menu-item" id="feedbacks-close" href="<?php echo get_home_url(); ?>#feedbacks">ОТЗЫВЫ</a></li>
            <li><a target-id="map2" class="nav_page drawer-menu-item" id="map2-close" href="<?php echo get_home_url(); ?>#map2">КОНТАКТЫ</a></li>
            <li><a class="nav_page_link drawer-menu-item" href="<?php echo get_page_link(18); ?>">НАШИ РАБОТЫ</a></li>
        </ul>
    </div>
</div>
<main role="main">	
    <!-- Page content -->
    <header class="header">
        <!-- Mobile header-->
        <div class="container-fluid hidden-md hidden-lg">
            <div class="row">
                <div class="col-xs-4 col-sm-4">
                    <div class="toolbar-icon toolbar-icon-left">

                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 logo-mobile-container">
                    <a href="<?php echo get_home_url(); ?>">
                        <div class="logo"></div>
                    </a>
                </div>
                <div class="col-xs-4 col-sm-4">
                    <div class="toolbar-icon toolbar-icon-right">
                        <a href="tel:+78182432843"><span class="glyphicon glyphicon glyphicon-earphone"></span></a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Desctop header-->
        <div class="container hidden-sm hidden-xs">
            <div class="row">
                <div class="col-md-8">
                    <div class="header-info">
                        <a href="<?php echo get_home_url(); ?>">
                            <div class="logo"></div>
                        </a>
                        <nav class="desctop-nav">
                            <a class="nav_page" target-id="main" href="<?php echo get_home_url(); ?>#main">ГЛАВНАЯ</a>
                            <a class="nav_page" target-id="spheres" href="<?php echo get_home_url(); ?>#spheres">УСЛУГИ</a>
                            <a class="nav_page" target-id="feedbacks" href="<?php echo get_home_url(); ?>#feedbacks">ОТЗЫВЫ</a>
                            <a class="nav_page" target-id="map2" href="<?php echo get_home_url(); ?>#map2">КОНТАКТЫ</a>
                            <a class="nav_page_link <?php echo is_page( 18 ) ? "current" : "" ?>" href="<?php echo get_page_link(18); ?>">НАШИ РАБОТЫ</a>
                        </nav>
                        <div id="is-front-page" class="hidden"><?php echo is_front_page() ? 'true' : 'false'; ?></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="wrapper">
                        <a href="" class="phone modal-call mb-hidden">8 (8182) <span>43-28-43</span></a>
                        <a href="tel:+78182432843" class="phone mb-visible">8 (8182) <span>43-28-43</span></a>
                        <div class="address">Архангельск, наб. Северной Двины, д. 55, оф. 309
                            <!--Архангельск, пр. Троицкий, 67, ТЦ «Пирамида», офис 504--></div>
                    </div>
                </div>
            </div>
        </div>

    </header>