$(document).ready(function () {

    if ($("#map").length) {
        var myMap;
        ymaps.ready(init);

        function init() {
            var myMap = new ymaps.Map('map', {
                    center: [64.531265, 40.526833],
                    zoom: 16
                }),
                myPlacemark = new ymaps.Placemark([64.531265, 40.526833], {}, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                    iconImageHref: 'img/map-marker.png',
                    // Размеры метки.
                    iconImageSize: [126, 167],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-63, -167]
                });

            myMap.controls
                .remove('mapTools')
                .remove('zoomControl')
                .remove('searchControl')
                .remove('typeSelector')
                .remove('geolocationControl')
                .remove('fullscreenControl')
                .remove('rulerControl')
                .remove('trafficControl');
            myMap.controls.add(new ymaps.control.ZoomControl({
                options: {
                    position: {
                        left: 10,
                        top: 170
                    }
                }
            }));
            myMap.geoObjects.add(myPlacemark);
            myMap.behaviors.disable("scrollZoom");
        }
    }

    $('header nav').singlePageNav({
        offset: $('header').outerHeight(),
        filter: ':not(.disabled)',
        updateHash: true,
    });

    $('.projects .slider').bxSlider({
        pager: false,
        prevText: '',
        nextText: '',
        slideWidth: 900,
        onSliderLoad: function (slideId, elem) {
            var classes = $(elem.viewport).parents('.bx-wrapper').attr('class');
            $(elem.viewport).parents('.bx-wrapper').addClass($(elem.viewport).find('.slider').attr('class').split(' ')[1]);
            if ($(elem.viewport).find('.slider').attr('class').split(' ').indexOf('hoa') != -1) {
                $(elem.viewport).parents('.bx-wrapper').show();
            } else {
                $(elem.viewport).parents('.bx-wrapper').hide();
            }
        },
    });

    // $('.clients .slider').bxSlider({
    //   minSlides: 3,
    //   maxSlides: 3,
    //   slideWidth: 245,
    //   slideMargin: 80,
    //   moveSlides: 1,
    //   pager: false,
    //   prevText: '',
    //   nextText: '',
    // });


    var $cont = $('0');
    if ($(window).width() < 1240) {
        $cont = 1;
    } else {
        $cont = 5;

    }

    $('.feedbacks .slider').bxSlider({
        minSlides: $cont,
        maxSlides: $cont,
        slideWidth: 180,
        moveSlides: 1,
        pager: false,
        prevText: '',
        nextText: '',
    });

    setTimeout(function () {
        $('header nav a, header .address, header .tel').each(function () {
            $(this).text($(this).text())
        });
    }, 600);

    $(window).on("load resize", function (e) {
        //$('header').width($(window).width());
    });

    $('#confidential').click(function (e) {
        e.preventDefault();
        $('#confidential-modal').arcticmodal();
    });

    function getActivTabSubject() {
        var $activTab = $("#spheres .tab.active");
        var subject = $activTab.find(".name").text();
        return subject;
    }

    function formSubmit(idForm) {
        switch (idForm) {
            case "contact-form-panorama":
                $("#contact-form-7-panorama-submit").click();
                break;
            case "contact-form-access":
                $("#contact-form-7-access-submit").click();
                break;
            case "contact-form-question":
                $("#contact-form-7-question-submit").click();
                break;
            case "contact-form-header":
                $("#contact-form-7-header-submit").click();
                break;
        }
        $("#success-modal").arcticmodal();
    }

    function contactFormSubmit(contactform) {

        var idForm = $(contactform).attr("id");
        var email;
        var phone;
        var text;
        var name;
        var subject;
        var city = "Город не определен";
        var region = "Регион не определен";

        switch (idForm) {
            case "contact-form-panorama":
                email = $("#contactform-panorama-email").val();
                phone = $("#contactform-panorama-phone").val();
                subject = $("#contactform-panorama-subject").val();
                $("#contact-form-7-panorama-email").val(email);
                $("#contact-form-7-panorama-phone").val(phone);
                $("#contact-form-7-panorama-city").val(city);
                $("#contact-form-7-panorama-region").val(region);
                $("#contact-form-7-panorama-subject").val(subject);
                break;
            case "contact-form-access":
                email = $("#contactform-access-email").val();
                phone = $("#contactform-access-phone").val();
                $("#contact-form-7-access-email").val(email);
                $("#contact-form-7-access-phone").val(phone);
                $("#contact-form-7-access-city").val(city);
                $("#contact-form-7-access-region").val(region);
                $("#contact-form-7-access-subject").val(getActivTabSubject());
                break;
            case "contact-form-question":
                text = $("#contactForm-question-text").val();
                phone = $("#contactForm-question-phone").val();
                $("#contact-form-7-question-text").val(text);
                $("#contact-form-7-question-phone").val(phone);
                $("#contact-form-7-question-city").val(city);
                $("#contact-form-7-question-region").val(region);
                break;
            case "contact-form-header":
                name = $("#contactForm-header-name").val();
                phone = $("#contactForm-header-phone").val();
                subject = $("#contactform-header-subject").val();
                $("#contact-form-7-header-name").val(name);
                $("#contact-form-7-header-phone").val(phone);
                $("#contact-form-7-header-city").val(city);
                $("#contact-form-7-header-region").val(region);
                $("#contact-form-7-header-subject").val(subject);
                break;
        }

        $.ajax({
            type: "GET",
            cache: false,
            url: "http://ip-api.com/json?lang=ru",
            success: function (data) {
                city = data.city;
                region = data.regionName;
                switch (idForm) {
                    case "contact-form-panorama":
                        $("#contact-form-7-panorama-city").val(city);
                        $("#contact-form-7-panorama-region").val(region);
                        break;
                    case "contact-form-access":
                        $("#contact-form-7-access-city").val(city);
                        $("#contact-form-7-access-region").val(region);
                        break;
                    case "contact-form-question":
                        $("#contact-form-7-question-city").val(city);
                        $("#contact-form-7-question-region").val(region);
                    break;
                        case "contact-form-header":
                        $("#contact-form-7-header-city").val(city);
                        $("#contact-form-7-header-region").val(region);
                        break;
                }
                formSubmit(idForm);
            },
            error: function (request, status, error) {
                formSubmit(idForm);
            }
        });
    }

    $(".contactform").on("submit", function (e) {
        var form_ya = $(this).data('ya');
        e.preventDefault();
        if ($(this).validationEngine('validate', {
                promptPosition: "topRight",
                scroll: false
            })) {
            var answer = $(this).serialize();
            $.arcticmodal('close');
            contactFormSubmit(this);
            this.reset();
        }
        return false;
    });

    $(".contactform input[type=text]").on('keydown', function () {
        $('.formError').animate({
            opacity: 0,
        }, 400, function () {
            $('.formError').remove();
        });
    });

    $('#success-modal .button').click(function () {
        $('#success-modal').arcticmodal('close');
    });

    $('.question .ask-button').click(function (e) {
        e.preventDefault();
        $('#question-modal').arcticmodal();
    });

    $(".modal-call").click(function (e) {
        e.preventDefault();
        $("#zvonok-modal").arcticmodal();
    });

    $('.feedbacks .slide').click(function () {
        $('#zoom-modal img').attr('src', $(this).data('big'));
        $('#zoom-modal').arcticmodal();
    });

    $('.spheres .tab').click(function () {
        var sphere = $(this).data('target');

        $('.spheres .tab').removeClass('active');
        $(this).addClass('active');

        var sections = ['.spheres .sphere', '.access .text', '.projects .bx-wrapper', '.clients .slider', '.why-we .reasons'];

        $.each(sections, function (index, value) {
            $(value).hide();
            $(value + '.' + sphere).show();
        });

        var new_action = $('.access .text:visible').text().toLowerCase();
        new_action = new_action.substr(0, 1).toUpperCase() + new_action.substr(1);
        $('.access form input[name=action]').val(new_action);
    });
});