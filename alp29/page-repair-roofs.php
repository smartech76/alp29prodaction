<?php

/*
Template Name: Страница Ремонт кровель
*/

get_header();
?>

<div class="panorama main repair-roofs-bg first-banner-element" id="main">
    <div class="main-substrate">
    </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-3 wrapper-block">
                    <div class="wrapper-width wrapper-width-md-small">
                        <ul class="left-menu">
                            <li class="paper-block">
                                <h3 class="text"><a href="<?php echo get_page_link(26); ?>">Утепление фасада</a></h3>
                            </li>
                            <li class="paper-block">
                                <h3 class="text"><a href="<?php echo get_page_link(28); ?>">Ремонт межпанельных швов</a></h3>
                            </li>
                            <li class="paper-block">
                                 <h3 class="text"><a href="<?php echo get_page_link(30); ?>">Ремонт кровель</a></h3>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 wrapper-block ">
                    <!-- empty block-->
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 wrapper-block ">
                    
                    <div class="row">
                        <div class="wrapper-width wrapper-middle wrapper-panorama-form bottom-margin">
                            <div class="wrapper">
                                <div class="row">
                                    <div class="icon"></div>
                                    <h1>УСЛУГИ ПРОМЫШЛЕННЫХ АЛЬПИНИСТОВ РАБОТЫ НА ВЫСОТЕ ЛЮБОЙ СЛОЖНОСТИ</h1>
                                    <div class="sub-title empty">
                                <span>
                                  <h2></h2>
                                </span>
                                    </div>
                                    <div class="text text-sale">Получите доступ к специальным ценам для ТСЖ и управляющих компаний,
                                        промышленных предприятий, государственных организаций и коммерческих фирм:
                                    </div>
                                </div>
                                <div class="row">
                                    <form class="contactform" id="contact-form-panorama">
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input id="contactform-panorama-email" type="text" name="email" placeholder="Email" class="validate[required,custom[email]] input-mobile">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <input id="contactform-panorama-phone" type="text" name="phone" placeholder="Телефон" class="validate[required,custom[phone],minSize[4]] input-mobile">
                                        </div>                                  
                                        <input type='hidden' name="action" value='Получите доступ к специальным ценам для ТСЖ и управляющих  компаний, промышленных предприятий, государственных организаций и коммерческих фирм'/>
                                        <input type='hidden' name="slide" value=""/>
                                        <input type='hidden' id="contactform-panorama-subject" name="contactform-panorama-subject" value='<?php get_mail_subject(); ?>'/>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <input type="submit" class="button red-button input-mobile" value="ПОЛУЧИТЬ ДОСТУП">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="row">
                                                <label class="white-link"><input type="checkbox" class="" id="access-personal-information">Я согласен на обработку персональных данных</label>
                                                <a href="" class="white-link">Текст соглашения</a>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="contact-form-7-wrapper" id="contact-form-7-panorama">
                                        <?php echo do_shortcode('[contact-form-7 id="12" title="ContactFormPanorama"]'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <?php
                    while ( have_posts() ) :
                        the_post();

                        get_template_part( 'template-parts/content', 'page' );

                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;

                    endwhile; // End of the loop.
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="question">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrapper">
                        <div class="section-title">У Вас остались вопросы?</div>
                        <h2>Задайте их нашему специалисту.</h2>
                        <div class="ask-button button red-button">ЗАДАТЬ ВОПРОС</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
